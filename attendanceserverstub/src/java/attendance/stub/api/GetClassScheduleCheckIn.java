/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.api;

import attendance.stub.util.ApiService;
import attendance.stub.util.DummyService;
import attendance.stub.util.ServletService;
import com.kmutt.cony.model.ClassSchedule;
import com.kmutt.cony.model.Student;
import com.kmutt.cony.model.TimeAttendance;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ess25_000
 */
public class GetClassScheduleCheckIn extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String[]param={"groupId","classScheduleId"};
        if(ServletService.hasParam(request,param)){
            try {
                int groupId=Integer.parseInt(request.getParameter("groupId"));
                int classScheduleId=Integer.parseInt(request.getParameter("classScheduleId"));
                ClassSchedule classScd=DummyService.getClassSchedule(classScheduleId);
                TimeAttendance timeAtd1=DummyService.getTimeAttendance();
                TimeAttendance timeAtd2=DummyService.getTimeAttendance();
                TimeAttendance timeAtd3=DummyService.getTimeAttendance();
                TimeAttendance timeAtd4=DummyService.getTimeAttendance();
                Student std1=DummyService.getStudent("S0001");
                Student std2=DummyService.getStudent("S0002");
                Student std3=DummyService.getStudent("S0003");
                Student std4=DummyService.getStudent("S0004");
                timeAtd1.setStudentId(std1.getUserId());
                timeAtd1.setStudent(std1);
                timeAtd2.setStudentId(std2.getUserId());
                timeAtd2.setStudent(std2);
                timeAtd3.setStudentId(std3.getUserId());
                timeAtd3.setStudent(std3);
                timeAtd4.setStudentId(std4.getUserId());
                timeAtd4.setStudent(std4);
                List<TimeAttendance>timeAtds=new ArrayList<TimeAttendance>();
                timeAtds.add(timeAtd1);
                timeAtds.add(timeAtd2);
                timeAtds.add(timeAtd3);
                timeAtds.add(timeAtd4);
                classScd.setTimeAttendance(timeAtds);
                out.print(ApiService.GSON.toJson(classScd));
            } catch (NumberFormatException e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }else{
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
