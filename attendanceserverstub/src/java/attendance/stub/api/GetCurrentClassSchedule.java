/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.api;

import attendance.stub.util.ApiService;
import attendance.stub.util.DateTimeService;
import attendance.stub.util.DummyService;
import attendance.stub.util.ServletService;
import com.kmutt.cony.model.ClassSchedule;
import com.kmutt.cony.model.Group;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author ess25_000
 */
public class GetCurrentClassSchedule extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String[]param={"date","time"};
        if(ServletService.hasParam(request,param)){
            String date=request.getParameter("date");
            String time=request.getParameter("time");
            try {
                if(!time.equals("0000")){
                    Date dateTime=DateTimeService.TIMESTAMP_FORMAT.parse(date+" "+time);
                    ClassSchedule classScd=DummyService.getClassSchedule(1);
                    Group group=DummyService.getGroup(1);
                    classScd.setGroupId(group.getGroupId());
                    classScd.setGroup(group);
                    out.print(ApiService.GSON.toJson(classScd));
                }else{
                    out.print(ApiService.GSON.toJson(null));
                }
                
            } catch (ParseException ex) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }else{
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
