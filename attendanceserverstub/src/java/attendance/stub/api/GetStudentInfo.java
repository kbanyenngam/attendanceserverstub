/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.api;

import attendance.stub.util.ApiService;
import attendance.stub.util.DummyService;
import attendance.stub.util.ServletService;
import com.kmutt.cony.model.ClassSchedule;
import com.kmutt.cony.model.Course;
import com.kmutt.cony.model.Group;
import com.kmutt.cony.model.Statistic;
import com.kmutt.cony.model.Student;
import com.kmutt.cony.model.TimeAttendance;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Administrator
 */
public class GetStudentInfo extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String[]param={"groupId","studentId"};
        if(ServletService.hasParam(request,param)){
            String studentId=request.getParameter("studentId");
            try {
                int groupId=Integer.parseInt(request.getParameter("groupId"));
                Student std=DummyService.getStudent(studentId);
                Group group=DummyService.getGroup(groupId);
                Statistic stat=DummyService.getStatistic();
                TimeAttendance timeAtd1=DummyService.getTimeAttendance();
                TimeAttendance timeAtd2=DummyService.getTimeAttendance();
                TimeAttendance timeAtd3=DummyService.getTimeAttendance();
                TimeAttendance timeAtd4=DummyService.getTimeAttendance();
                ClassSchedule classScd1=DummyService.getClassSchedule(1);
                ClassSchedule classScd2=DummyService.getClassSchedule(2);
                ClassSchedule classScd3=DummyService.getClassSchedule(3);
                ClassSchedule classScd4=DummyService.getClassSchedule(4);
                timeAtd1.setClassSchedule(classScd1);
                timeAtd1.setClassScheduleId(classScd1.getClassScheduleId());
                timeAtd2.setClassSchedule(classScd2);
                timeAtd2.setClassScheduleId(classScd2.getClassScheduleId());
                timeAtd3.setClassSchedule(classScd3);
                timeAtd3.setClassScheduleId(classScd3.getClassScheduleId());
                timeAtd4.setClassSchedule(classScd4);
                timeAtd4.setClassScheduleId(classScd4.getClassScheduleId());
                List<TimeAttendance>timeAtds=new ArrayList<TimeAttendance>();
                timeAtds.add(timeAtd1);
                timeAtds.add(timeAtd2);
                timeAtds.add(timeAtd3);
                timeAtds.add(timeAtd4);
                stat.setGroupId(group.getGroupId());
                stat.setGroup(group);
                stat.setStudentId(std.getUserId());
                stat.setStudent(std);
                stat.setTimeAttendance(timeAtds);
                out.print(ApiService.GSON.toJson(stat));
            } catch (NumberFormatException e) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            }
        }else if(ServletService.hasParam(request,param[1])){
            String studentId=request.getParameter("studentId");
            Student std=DummyService.getStudent(studentId);
            Group group1=DummyService.getGroup(1);
            Group group2=DummyService.getGroup(2);
            Group group3=DummyService.getGroup(3);
            Group group4=DummyService.getGroup(4);
            Course course1=DummyService.getCourse(1);
            Course course2=DummyService.getCourse(2);
            Course course3=DummyService.getCourse(3);
            Course course4=DummyService.getCourse(4);
            group1.setCourseId(course1.getCourseId());
            group1.setCourse(course1);
            group2.setCourseId(course2.getCourseId());
            group2.setCourse(course2);
            group3.setCourseId(course3.getCourseId());
            group4.setCourse(course3);
            group4.setCourseId(course4.getCourseId());
            group4.setCourse(course4);
            List<Group>groups=new ArrayList<Group>();
            groups.add(group1);
            groups.add(group2);
            groups.add(group3);
            groups.add(group4);
            std.setRegistered(groups);
            out.print(ApiService.GSON.toJson(std));
        }else{
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
