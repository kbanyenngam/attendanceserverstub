/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.util;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author Administrator
 */
public class AuthenticationService {
    public static void httpAuthen(HttpServletRequest request,HttpServletResponse response,FilterChain chain) throws IOException, ServletException{
        String authenValue=request.getHeader("Authorization");
        System.out.println("///////////////"+authenValue);
        if(authenValue!=null){
            String[]a=authenValue.split(" ");
            if(a!=null&&a.length==2&&a[0].equalsIgnoreCase("basic"))
                basicAuthen(a[1],request,response,chain);
            else
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }else{
            response.sendError(HttpServletResponse.SC_FORBIDDEN);
        }
        
    }
    public static void basicAuthen(String raw,HttpServletRequest request,HttpServletResponse response,FilterChain chain) throws IOException, ServletException{
        String authenValue=new String(Base64.decodeBase64(raw));
        String[]a=authenValue.split("\\:");
        if(a!=null&&a.length==2)
            doAuthentication(a[0],a[1],request,response,chain);
        else
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        
    }
    public static void doAuthentication(String username,String password,HttpServletRequest request,HttpServletResponse response,FilterChain chain) throws IOException, ServletException{
        String USERNAME=request.getSession().getServletContext().getInitParameter("USERNAME");
        String PASSWORD=request.getSession().getServletContext().getInitParameter("PASSWORD");
        if(username.equals(USERNAME)&&password.equals(PASSWORD))
            chain.doFilter((ServletRequest)request,(ServletResponse)response);
        else
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
