/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ess25_000
 */
public class DateTimeService {
    public static final SimpleDateFormat DATE_FORMAT=new SimpleDateFormat("ddMMYYYY");
    public static final SimpleDateFormat TIME_FORMAT=new SimpleDateFormat("HHmm");
    public static final SimpleDateFormat TIMESTAMP_FORMAT=new SimpleDateFormat("ddMMYYYY HHmm");
    public static final SimpleDateFormat GSON_TIMESTAMP_FORMAT=new SimpleDateFormat("YYYY-MM-dd HH:mm -Z");
}
