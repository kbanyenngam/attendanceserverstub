/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package attendance.stub.util;

import com.kmutt.cony.model.ClassSchedule;
import com.kmutt.cony.model.Course;
import com.kmutt.cony.model.Group;
import com.kmutt.cony.model.Statistic;
import com.kmutt.cony.model.Student;
import com.kmutt.cony.model.Teacher;
import com.kmutt.cony.model.TimeAttendance;
import java.util.Date;

/**
 *
 * @author ess25_000
 */
public class DummyService {
    public static Student getStudent(String i){
        Student std=new Student();
        std.setUserId(i);
        std.setLastName("last_name_"+i);
        std.setFirstName("first_name_"+i);
        std.setAddress("address_"+i);
        std.setPhoneNumber(String.valueOf(Math.random()).replace(".","").substring(0,10));
        std.setPhoto("http://kmutt.cony/"+i+".png");
        return std;
    }
    public static Teacher getTeacher(String i){
        Teacher tch=new Teacher();
        tch.setUserId(i);
        tch.setLastName("last_name_"+i);
        tch.setFirstName("first_name_"+i);
        tch.setAddress("address_"+i);
        tch.setPhoneNumber(String.valueOf(Math.random()).replace(".","").substring(0,10));
        tch.setPhoto("http://kmutt.cony/"+i+".png");
        return tch;
    }
    public static Statistic getStatistic(){
        Statistic stat=new Statistic();
        stat.setAbsence((int)(Math.random()*10));
        stat.setPresent((int)(Math.random()*10));
        stat.setLate((int)(Math.random()*10));
        stat.setUnknow((int)(Math.random()*10));
        return stat;
    }
    public static Group getGroup(int i){
        Group group=new Group();
        group.setGroupId(i);
        group.setClassroom("ROOM 00"+i);
        group.setDescription("Group "+i);
        return group;
    }
    public static TimeAttendance getTimeAttendance(){
        TimeAttendance timeAtd=new TimeAttendance();
        timeAtd.setCheckInTime(new Date());
        timeAtd.setStatus((int)(Math.random()*4)+1);
        return timeAtd;
    }
    public static ClassSchedule getClassSchedule(int id){
        ClassSchedule classScd=new ClassSchedule();
        classScd.setClassScheduleId(id);
        classScd.setDateTime(new Date());
        classScd.setDescription("class at "+classScd.getDateTime().toString());
        classScd.setPeriod((int)(Math.random()*3)+1);
        return classScd;
    }
    public static Course getCourse(int i){
        Course course=new Course();
        course.setCourseId(i);
        course.setSubject("SWE10"+i);
        course.setDescription("Course "+course.getSubject());
        return course;
    }
}
