/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.kmutt.cony.model;

import java.util.List;

/**
 *
 * @author ess25_000
 */
public class Student extends User{
    private List<Statistic>statistic;
    public Student(){super();}
    public List<Statistic> getStatistic() {
        return statistic;
    }
    public void setStatistic(List<Statistic> statistic) {
        this.statistic = statistic;
    }
}
