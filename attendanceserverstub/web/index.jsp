<%-- 
    Document   : index
    Created on : Aug 21, 2014, 1:51:04 AM
    Author     : Administrator
--%>

<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    List apiName=new ArrayList();
    List apiParam=new ArrayList();
    List apiPath=new ArrayList();
    List apiNoAuthenPath=new ArrayList();
    List apiExamplePath=new ArrayList();
    List apiDescription=new ArrayList();
    
    apiName.add("GetMyInfo");
    apiParam.add("no param");
    apiPath.add("jsonService/getMyInfo");
    apiNoAuthenPath.add("GetMyInfo");
    apiExamplePath.add("GetMyInfo");
    apiDescription.add("ข้อมูลผู้สอนที่ login อยู่");
    
    apiName.add("GetCourseList");
    apiParam.add("no param");
    apiPath.add("jsonService/getCourseList");
    apiNoAuthenPath.add("GetCourseList");
    apiExamplePath.add("GetCourseList");
    apiDescription.add("รายวิชาและกลุ่มย่อยทั้งหมดของผู้สอนมีอยู่ในเทอมการศึกษานี้");
    
    apiName.add("GetClassSchedule");
    apiParam.add("groupId={number}");
    apiPath.add("jsonService/getClassSchedule");
    apiNoAuthenPath.add("GetClassSchedule");
    apiExamplePath.add("GetClassSchedule?groupId=1");
    apiDescription.add("Schedule ทั้งหมดของกลุ่ม <br> dateTime จะอยู่ในรูปแบบ YYYY-MM-dd HH:mm -Z");
    
    apiName.add("GetClassScheduleCheckIn");
    apiParam.add("groupId={number}&classScheduleId={number}");
    apiPath.add("jsonService/getClassScheduleCheckIn");
    apiNoAuthenPath.add("GetClassScheduleCheckIn");
    apiExamplePath.add("GetClassScheduleCheckIn?groupId=1&classScheduleId=1");
    apiDescription.add("รายชื่อนักศึกษาที่เข้าเรียนในวันนั้น <br>status 1=Present, <br>2=Late, 3=Absence, 4=Unknow When one of student has checked-in, other student status must be “Absence”.");
    
    apiName.add("GetStudentList");
    apiParam.add("groupId={number}");
    apiPath.add("jsonService/getStudentList");
    apiNoAuthenPath.add("GetStudentList");
    apiExamplePath.add("GetStudentList?groupId=1");
    apiDescription.add("รายชื่อนักศึกษาในกลุ่ม และจำนวนการเข้าเรียน แยกตามสถานะ");
    
    apiName.add("GetStudentInfo");
    apiParam.add("studentId={text}&groupId={number}");
    apiPath.add("jsonService/getStudentList");
    apiNoAuthenPath.add("GetStudentInfo");
    apiExamplePath.add("GetStudentInfo?studentId=S0001&groupId=1");
    apiDescription.add("ข้อมูลนักศึกษาและประวัติการเข้าเรียนของกลุ่มนั้น");
    
    apiName.add("GetStudentInfo");
    apiParam.add("studentId={text}");
    apiPath.add("jsonService/getStudentList");
    apiNoAuthenPath.add("GetStudentInfo");
    apiExamplePath.add("GetStudentInfo?studentId=S0001");
    apiDescription.add("ข้อมูลของนักศึกษาและรายชื่อวิชาและกลุ่มที่เรียนอยู่");
    
    apiName.add("GetCurrentClassSchedule");
    apiParam.add("date={ddMMYYYY}&time={HHmm}");
    apiPath.add("jsonService/getCurrentClassSchedule");
    apiNoAuthenPath.add("GetCurrentClassSchedule");
    apiExamplePath.add("GetCurrentClassSchedule?date=09092014&time=1600");
    apiDescription.add("schedule ที่ใกล้จะเริ่มหรือกำลังอยู่ในช่วงเวลา <br> ถ้าไม่พบจะเป็น null ลองใส่ time เป็น 0000 ดู (<a href=\"GetCurrentClassSchedule?date=09092014&time=0000\" target=\"_blank\">GetCurrentClassSchedule?date=09092014&time=0000</a>)");
    
    apiName.add("GetUserInfo");
    apiParam.add("faceId={text}");
    apiPath.add("GetUserInfo");
    apiNoAuthenPath.add("GetUserInfo");
    apiExamplePath.add("GetUserInfo?faceId=1");
    apiDescription.add("");
    
    pageContext.setAttribute("apiName",apiName);
    pageContext.setAttribute("apiParam",apiParam);
    pageContext.setAttribute("apiPath",apiPath);
    pageContext.setAttribute("apiNoAuthenPath",apiNoAuthenPath);
    pageContext.setAttribute("apiExamplePath",apiExamplePath);
    pageContext.setAttribute("apiDescription",apiDescription);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Attendance Server Stub</title>
    </head>
    <body>
        <h1>Attendance Server Stub</h1>
        <h4>Basic Authentication</h4>
        <dl>
            <dt><strong>username:</strong></dt>
            <dd>kmutt</dd>
        </dl>
        <dl>
            <dt><strong>password:</strong></dt>
            <dd>cony</dd>
        </dl>
        <hr>
        <h4>Available API</h4>

        <ol>
            <%
                for(int i=0;i<apiName.size();i++){
                    out.print("<li>");
                        out.print("<strong>"+apiName.get(i)+"</strong>[<em>"+apiParam.get(i)+"</em>]<br>");
                        out.print("<strong>path:</strong> <a href='"+apiPath.get(i)+"' target='_blank'>"+apiPath.get(i)+"</a><br>");
                        out.print("<strong>without authen:</strong> <a href='"+apiNoAuthenPath.get(i)+"' target='_blank'>"+apiNoAuthenPath.get(i)+"</a><br>");
                        out.print("<strong>example:</strong> <a href='"+apiExamplePath.get(i)+"' target='_blank'>"+apiExamplePath.get(i)+"</a><br>");
                        out.print("<em>"+apiDescription.get(i)+"</em>");
                        out.print("<br>");
                        out.print("<br>");
                        out.print("<br>");
                    out.print("</li>");
                }
            %>
        </ol>
            
    </body>
</html>
